# React Tabbed Bookings

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). State management of bookings and tabs is handled using the React Context API.


## Getting started

Clone the git repo and in the project root run

```npm install```

followed by

```npm run start```


## Running tests

Run and watch tests
```npm run test```
