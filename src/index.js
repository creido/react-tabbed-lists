import React from 'react';
import ReactDOM from 'react-dom';

import './css/styles.css'

import App from './layout/App';

ReactDOM.render(<App />, document.getElementById('root'));
