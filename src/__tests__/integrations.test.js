import React from 'react'
import {mount} from 'enzyme'
import App from '../layout/App'

describe('App', () => {

  let wrapper

  beforeEach(() => {
    wrapper = mount(<App />)
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('shows a list of bookings by default', () => {
    expect(wrapper.find('.booking').length).toEqual(4)
    expect(wrapper.find('.tabs__tab-title').text()).toBe('Pending')
  })

  it('should change to Confirmed tab on clicking last tab link and then revert to Pending tab on clicking the first tab link', () => {
    wrapper.find('.tabs__nav-link').last().simulate('click')
    wrapper.update()
    expect(wrapper.find('.booking').length).toEqual(2)
    expect(wrapper.find('.tabs__tab-title').text()).toBe('Confirmed')

    wrapper.find('.tabs__nav-link').first().simulate('click')
    wrapper.update()
    expect(wrapper.find('.booking').length).toEqual(4)
    expect(wrapper.find('.tabs__tab-title').text()).toBe('Pending')
  })

  it('should move a pending booking to the confirmed tab on clicking \'accept\' button', () => {
    wrapper.find('.booking').first().find('button').simulate('click')
    wrapper.update()
    expect(wrapper.find('.booking')).toHaveLength(3)
  })

  it('should remove a confirmed booking completely on clicking \'cancel\' button', () => {
    wrapper.find('.tabs__nav-link').last().simulate('click')
    wrapper.update()
    wrapper.find('.booking').first().find('button').simulate('click')
    wrapper.update()
    expect(wrapper.find('.booking')).toHaveLength(1)
  })

})
