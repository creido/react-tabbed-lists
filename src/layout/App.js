import React from 'react'

import BookingsContainer from '../components/Bookings/BookingsContainer'

const App = () => {
  const BOOKING_DATA = [
    {
      "id": "1",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "pending"
    },
    {
      "id": "2",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "confirmed"
    },
    {
      "id": "3",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "pending"
    },
    {
      "id": "4",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "pending"
    },
    {
      "id": "5",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "confirmed"
    },
    {
      "id": "6",
      "avatar": "img/avatar.jpg",
      "firstname": "Kenneth",
      "lastname": "L",
      "startTime": "Sat 20 Oct, 6:30 pm",
      "status": "pending"
    }
  ]

  return <main className="app-main">
    <BookingsContainer data={BOOKING_DATA}/>
  </main>
}

export default App
