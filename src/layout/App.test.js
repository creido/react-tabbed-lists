import React from 'react'
import {shallow} from 'enzyme'

import App from './App'
import BookingsContainer from '../components/Bookings/BookingsContainer'

describe('App component', () => {
  it('shows the BookingsContainer', () => {
    const wrapper = shallow(<App />)

    expect(wrapper.find(BookingsContainer).length).toEqual(1)
  });

});
