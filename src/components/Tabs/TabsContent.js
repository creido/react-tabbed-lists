import React, {Component} from 'react'

import {TabsContext} from './TabsContext';

class TabsContent extends Component {

  componentDidMount() {
    // get a reference for each child component
    const tabs = this.props.children.length
      ? this.props.children.map(childItem =>
        ({
          id: childItem.props.id,
          title: childItem.props.title
        }))
      : []


    // add references to main component context for navigation
    this.props.context.getTabs(tabs)
  }

  render() {
    return this.props.children
  }
}

export default React.forwardRef((props, ref) => (
  <TabsContext.Consumer>
    {
      context =>
        <TabsContent {...props} context={context} ref={ref} />
    }
  </TabsContext.Consumer>
))
