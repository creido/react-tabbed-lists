import React from 'react'

import {TabsContext} from './TabsContext'

const TabsNavLink = ({id, title}) =>
  <TabsContext.Consumer>
    {context => {
      const isActive = id === context.activeTab
      const tabClasses = isActive
        ? 'tabs__nav-link tabs__nav-link--active'
        : 'tabs__nav-link'

      return (
        <li
          onClick={() => context.setActiveTab(id)}
          className={tabClasses}>
          {title}
        </li>
      )
    }}
  </TabsContext.Consumer>

export default TabsNavLink
