import {createContext} from 'react'

const defaultState = {}

export const TabsContext = createContext(defaultState)
