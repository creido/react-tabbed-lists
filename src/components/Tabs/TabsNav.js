import React from 'react'

import {TabsContext} from './TabsContext'
import TabsNavLink from './TabsNavLink'

const TabsNav = () =>
  <TabsContext.Consumer>
    {context => (
      <nav className="tabs__nav">
        <ol className="tabs__nav-list">
          {
            context.tabs.map(tab =>
              <TabsNavLink key={tab.title} {...tab} />
            )
          }
        </ol>
      </nav>
    )}
  </TabsContext.Consumer>

export default TabsNav
