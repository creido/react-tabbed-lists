import React, {Component} from 'react'

import {TabsContext} from './TabsContext'
import TabsContent from './TabsContent'
import TabsNav from './TabsNav'
import Tab from './Tab'

class Tabs extends Component {
  static Tab = Tab
  static TabsContent = TabsContent
  static TabsNav = TabsNav

  state = {
    activeTab: 'pending',
    tabs: [],
    setActiveTab: id => {
      return this.setState(prevState =>
        id !== prevState.activeTab
        && {activeTab: id}
      )
    },
    // get references for individual tab elements
    getTabs: tabs => {
      return this.setState(prevState => {
        return {tabs: [...prevState.tabs, ...tabs]}
      })
    },
  }

  render() {
    return (
      <TabsContext.Provider value={this.state} activeTab={this.props.activeTab}>
        <div className="tabs">
          {this.props.children}
        </div>
      </TabsContext.Provider>
    )
  }
}

export default Tabs
