import React from 'react';

import {TabsContext} from './TabsContext';

const Tab = ({id, title, children}) => (
  <TabsContext.Consumer>
    {context => {
      // could also use css classes to visually show and hide tabs
      // whilst leaving DOM largely untouched

      return id === context.activeTab && (
        <div id={id} className="tabs__tab">
          <h2 className="tabs__tab-title hidden">{title}</h2>
          {children}
        </div>
      )
    }}
  </TabsContext.Consumer>
)

export default Tab
