import React from 'react'
import {shallow, mount, render} from 'enzyme'

import Booking from './Booking'

const mockPending = {
  id: '1',
  avatar: '',
  firstname: 'John',
  lastname: 'Smith',
  startTime: '01 Jan 2018',
  status: 'pending',
}

const mockConfirmed = {
  id: '2',
  avatar: '',
  firstname: 'John',
  lastname: 'Smith',
  startTime: '01 Jan 2018',
  status: 'confirmed',
}

const mockCancelled = {
  id: '3',
  avatar: '',
  firstname: 'John',
  lastname: 'Smith',
  startTime: '01 Jan 2018',
  status: 'cancelled'
}

describe('Pending booking component', () => {
  let wrapper

  it('has \'Accept\' CTA', () => {
    wrapper = mount(<Booking {...mockPending} />)
    expect(wrapper.find('button').text()).toBe('Accept')
    expect(wrapper.prop('status')).toBe('pending')
  })

  it('should match its snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  })

})

describe('Confirmed booking component', () => {
  let wrapper

  it('has \'Cancel\' CTA', () => {
    wrapper = mount(<Booking {...mockConfirmed} />)
    expect(wrapper.find('button').text()).toBe('Cancel')
    expect(wrapper.prop('status')).toBe('confirmed')
  })

  it('should match its snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  })
});

describe('Cancelled booking component', () => {
  let wrapper

  it('has no CTA', () => {
    wrapper = mount(<Booking {...mockCancelled} />)
    expect(wrapper.find('button')).toHaveLength(0)
    expect(wrapper.prop('status')).toBe('cancelled')
  })

  it('should match its snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  })
});
