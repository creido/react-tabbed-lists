import React, {Component} from 'react'

import {BookingsContext} from './BookingsContext'
import List from './List'
import Tabs from '../Tabs/Tabs'

class BookingsContainer extends Component {
  state = {
    items: [],
    onStatusChange: (id, newStatus) => this.setState(prevState => {
      return {
        items: [...prevState.items.map(item => item.id === id
          ? { ...item, status: newStatus }
          : item)
        ]
      }
    })
  }

  componentDidMount() {
    /**
     * Initial state set using this lifecycle method as a stepping
     * stone towards fetching the data asynchronously
     */

    // get initial content from props
    this.setState((prevState, props) => {
      return {
        items: [
          ...prevState.items,
          ...props.data
        ]
      }
    })
  }

  render() {
    return (
      <BookingsContext.Provider value={this.state}>
        <section className="section">
          {/*<h1>My Bookings</h1>*/}
          <Tabs>
            {
              /**
               * Tabs structure done in this way for max flexibility
               * Allows for moving nav underneath content tabs
               */
            }
            <Tabs.TabsNav />

            <Tabs.TabsContent>
              <Tabs.Tab id="pending" title="Pending">
                <List items={this.state.items} filter="pending" className="booking-list" />
              </Tabs.Tab>

              <Tabs.Tab id="confirmed" title="Confirmed">
                <List items={this.state.items} filter="confirmed" className="booking-list" />
              </Tabs.Tab>
            </Tabs.TabsContent>
          </Tabs>
        </section>

      </BookingsContext.Provider>
    )
  }
}

export default BookingsContainer
