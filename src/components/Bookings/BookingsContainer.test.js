import React from 'react'
import {shallow} from 'enzyme'

import BookingsContainer from './BookingsContainer'

describe('BookingsContainer', () => {
  // Work in progress
  let wrapper

  const mockData = []

  beforeEach(() => {
    wrapper = shallow(<BookingsContainer data={mockData}/>)
  })

  it.skip('renders correctly', () => {
    expect(wrapper.length).toEqual(1)
    expect(wrapper.props.children.length).toEqual(0)
  });

});
