import {createContext} from 'react'

const defaultState = {}

export const BookingsContext = createContext(defaultState)
