import React from 'react'

import {BookingsContext} from './BookingsContext';
import Button from '../Button';

const Booking = ({
        id,
        avatar,
        firstname,
        lastname,
        startTime,
        status,
      }) => {

  return <BookingsContext.Consumer>
    {context => (
      <li
        className={`booking booking--${status}`}
        data-id={id}>
        {
          /* TODO: extract booking details into sub components */
        }
        <div className="booking__avatar">
          <img src={avatar} alt="" className="booking__avatar-image" />
          <span className={`booking__status booking__status--${status}`}>
            <span className="hidden">{status}</span>
          </span>
        </div>

        <div className="booking__info">
          <h3 className="booking__name">{firstname} {lastname}</h3>
          <div className="booking__start-time">{startTime}</div>
        </div>

        <div className="booking__buttons">
          <ButtonConfirm
            isActive={status === 'pending'}
            onConfirm={() => context.onStatusChange(id, 'confirmed')} />
          <ButtonCancel
            isActive={status === 'confirmed'}
            onCancel={() => context.onStatusChange(id, 'cancelled')} />
        </div>
      </li>
      )
    }
  </BookingsContext.Consumer>
}

export default Booking

/**
 * TODO: investigate suspected issue with whitespace
 * to enable rendering of Booking as table row
 *
 * ```
 * return (
 *   <tr data-id={id}>
 *     <td><img src={avatar} alt="" /> {status} </td>
 *     <td>{firstname} {lastname}</td>
 *     <td>{startTime}</td>
 *     <td>
 *       {buttonConfirm}
 *       {buttonCancel}
 *     </td>
 *   </tr>
 * )
 * ```
 *
 */


export const ButtonConfirm = ({isActive, onConfirm}) =>
  isActive && <BookingsContext.Consumer>
    {context =>
      <Button
        className="btn btn--primary"
        onClick={onConfirm}>
        Accept
      </Button>
    }
  </BookingsContext.Consumer>

export const ButtonCancel = ({isActive, onCancel}) =>
  isActive && <BookingsContext.Consumer>
    {context =>
      <Button
        className="btn btn--secondary"
        onClick={onCancel}>
        Cancel
      </Button>
    }
  </BookingsContext.Consumer>
