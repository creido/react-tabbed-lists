import React from 'react'

import Booking from './Booking'

const getListItems = (items, filterName) => {
  if (!items || !items.length) return

  // if no filter specified return all items as components
  if (!filterName) {
    return items.map(item =>
      <Booking key={item.id} {...item} />)
  }

  // return items with specified filter
  return items
          .filter(item => item.status === filterName)
          .map(item => <Booking key={item.id} {...item} />)
}

const List = ({items, filter, className}) => {
  /**
   * TODO: introduce render prop or HOC for scalability and reuse
   * allowing for child components other than <Booking /> to be displayed
   */

  const listItems = getListItems(items, filter)

  return listItems && listItems.length
    ? <ul className={className}>{listItems}</ul>
    : <div className="tab__message--empty">No results</div>

  /**
   * originally this component rendered tabular data
   *
   * ```
   * return <table><tbody>{listItems}</tbody></table>
   * ```
   *
   * however React is stubbornly insisting
   * index.js:2178 Warning: validateDOMNesting(...):
   * Text nodes cannot appear as a child of <tbody>
   *
   * This issue is somehow caused by whitespace even after
   * all whitespace has been removed the error still appears
   * and prevents effective testing
   */
}

export default List
